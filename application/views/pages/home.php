<?php $this->load->helper('url'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>MindGeek</title>
</head>
<body>
	<button id="fetchData" role="button">Fetch Data</button>
	<input type="hidden">
	<pre>
	<div id="test"></div>

	<script type="text/javascript">

		const fetchData = document.getElementById('fetchData');
		fetchData.addEventListener('click', ajaxCall);


		function ajaxCall(){

			const proxyurl = "https://cors-anywhere.herokuapp.com/";
			const url = "https://mgtechtest.blob.core.windows.net/files/showcase.json";

			const xhr = new XMLHttpRequest();

			xhr.open('GET', proxyurl + url, true);
			xhr.onload = function(){
				if(this.status = 200){
					sendJSONToController(this.responseText);
				}
			}

			xhr.onerror = function(){

				console.log('AJAX request error');
			}

			xhr.send();
		}

		function sendJSONToController(data){

			const xhr = new XMLHttpRequest();
			
			xhr.open('POST', '<?php echo base_url(); ?>index.php/home/parse', true);
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
			xhr.send(data);
			xhr.onload = function(){

				if(this.status == 200){

					document.getElementById('test').innerHTML = this.responseText;
				}
			}

			xhr.onerror = function(){
				console.log('couldn\'t send data to the controller')
			};

			
		}





	</script>
</body>
</html>